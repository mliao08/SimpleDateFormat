import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

//import org.omg.CORBA.SystemException;

public class Test {
	public void test() {
//		String test1 = "19740625";
//		String test2 = "201112311039";
//		String test3 = "2015-02-24";
//		String test4 = "2016-01-01 01:02:03";
//		String test5 = "2016-12-07 10:20:13.55555555";
//		String test6 = "2016-2-11.8.11.52.0";
//		String test7 = "2016-12-24.17.47.48.66164000";
//		String test8 = "2016-06-24 11:43";
//		String test9 = "2016-06-24&&&13:33:11";
//		String test10 = "2008.8.4 pm 13:37:05";
//		String test11 = "30/07/2017 1:23:42";
//		String test12 = "27-8�� -17";
//		String test13 = "2008-03-05.15.21.55.55555555";
		String test14 = "2008-09-10&&&&12:27:09";
		
		String pattern1 = "yyyyMMdd";
		String pattern2 = "yyyyMMddHHmm";
		String pattern3 = "yyyy-MM-dd";
		String pattern4 = "yyyy-MM-dd HH:mm:ss";
		String pattern5 = "yyyy-MM-dd HH:mm:ss.S";
		String pattern6 = "yyyy-MM-dd.HH.mm.ss.S";
		String pattern7 = "yyyy-MM-dd.HH.mm.ss";
		String pattern8 = "yyyy-MM-dd HH:mm";
		String pattern9 = "yyyy.MM.dd a hh:mm";
		String pattern10 = "dd/MM/yyyy HH:mm:ss";
		String pattern11 = "dd-MM�� -yy";
		String pattern12 = "yyyy-MM-dd.HH.mm.ss";
		String pattern13 = "yyyy-MM-dd.HH.mm.ss";
		
		List<String> testStrings = new LinkedList<String>();
//		testStrings.add(test1);
//		testStrings.add(test2);
//		testStrings.add(test3);
//		testStrings.add(test4);
//		testStrings.add(test5);
//		testStrings.add(test6);
//		testStrings.add(test7);
//		testStrings.add(test8);
//		testStrings.add(test9);
//		testStrings.add(test10);
//		testStrings.add(test11);
//		testStrings.add(test12);
//		testStrings.add(test13);
		testStrings.add(test14);
		
		List<String> testPatterns = new LinkedList<String>();
		testPatterns.add(pattern1);
		testPatterns.add(pattern2);
		testPatterns.add(pattern3);
		testPatterns.add(pattern4);
		testPatterns.add(pattern5);
		testPatterns.add(pattern6);
		testPatterns.add(pattern7);
		testPatterns.add(pattern8);
		testPatterns.add(pattern9);
		testPatterns.add(pattern11);
		testPatterns.add(pattern12);
		testPatterns.add(pattern13);
		
		for(String pattern:testPatterns) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern,Locale.ENGLISH);
			for( String test:testStrings ){
				try {
					Date date = sdf.parse(test);
					if( null != date ) {
						System.out.println("pattern="+pattern+";  testString="+test+";  date="+date.toString());
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					System.out.println("[ERROR]"+"  pattern="+pattern+";  testString="+test);
				}
			}
		}
	}
}
